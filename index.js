class Student {
	constructor(name, email, grades) {
		//propertyName = value
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;
		//Function Coding 2:
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
	}
	willPass() {
		//return this.computeAve() >= 85 ? true : false;
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		// if(this.willPass()) {
		// 	if(this.computeAve() >= 90) {
		// 		return true;
		// 	} else {
		// 		return false;
		// 	}
		// } else {
		// 	return undefined;
		// }
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}



class Section {
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorsPercentage = undefined;
	}

	// method for adding a student to section

	addStudent(name, email, grades){
		this.students.push(new Student(name, email, grades));
		return this;
	}

	countHonorStudents(){

		let count = 0;

		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}

		})


		this.honorStudents = count;
		return this;

	}

	computeHonorsPercentage(){

		this.honorsPercentage = (this.honorStudents / this.students.length) *100;

		return this;

	}
}
/*
const section1A = new Section ('section1A')
console.log(section1A);
*/


/*section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);

section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);

section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);

section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93])
*/
/*console.log(section1A);

console.log(section1A.countHonorStudents())

console.log(section1A.computeHonorsPercentage());
*/

// Activity - Function Coding 

class Grade {
	constructor(number){
		this.level = (typeof number == 'number')? number : undefined;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(section){
		this.sections.push(new Section(section));
		return this;
	}

	countStudents(){
		let count = 0;
		this.sections.forEach(section=> {
			section.students.forEach(student=> {
				count++;
			});
		})
		this.totalStudents = count;
		return this;
	}

	countHonorStudents(){

		let count = 0;

		this.sections.forEach(section=> {
			section.students.forEach(student=> {
				student.computeAve();
				student.willPass();
				student.willPassWithHonors();
				if(student.passedWithHonors){
					count++;
				}
			});
		})
		this.totalHonorStudents = count;
		return this;
	}

	computeBatchAve(){

		let totalAve = 0;

		this.sections.forEach(section=> {
			section.students.forEach(student=> {
				
				totalAve += student.gradeAve;

			});
		})

		this.batchAveGrade = totalAve / this.totalStudents;
	}

	getBatchMinGrade(){

		let lowest = 100;

		this.sections.forEach(section=> {
			section.students.forEach(student=> {
				student.grades.forEach(grade=> {
					if(grade < lowest){
						lowest = grade;
					}
				})
			})
		})

		this.batchMinGrade = lowest;
	}

	getBatchMaxGrade(){

		let highest = 0;

		this.sections.forEach(section=> {
			section.students.forEach(student=> {
				student.grades.forEach(grade=> {
					if(grade > highest){
						highest = grade;
					}
				})
			})
		})

		this.batchMaxGrade = highest;
	}
}

// Number 1
const grade1 = new Grade(1);

// Number 2
grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");


//  Number 3
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");


section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);


// Number 4
grade1.countStudents();


//  Number 5
grade1.countHonorStudents();


//  Number 6
grade1.computeBatchAve();


// Number 7
grade1.getBatchMinGrade();

// Number 8
grade1.getBatchMaxGrade();

console.log(grade1);